# Base image:
FROM golang:1.13

# Install golint
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:$PATH
RUN go get -u golang.org/x/lint/golint

# Add apt key for LLVM repository
#RUN apt-key add -
#RUN wget -O - | apt-key add -
#RUN cat /etc/apt/sources.list

# Add LLVM apt repository
#RUN echo "deb llvm-toolchain-stretch-5.0 main" | tee -a /etc/apt/sources.list

# Install clang from LLVM repository
RUN apt-get update && apt-get install -y --no-install-recommends \
    clang-6.0 xterm \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN echo "export CC=clang-6.0" | tee -a ${set_clang} && chmod a+x ${set_clang}
