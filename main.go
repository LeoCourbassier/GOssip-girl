package main

func main() {
	CreateWriter()
	LoadUsers()
	LoadChannels()
	LoadBehaviours()
	InitializeServer()
}
