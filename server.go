package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
)

// VERSION is the server version
const VERSION = "1.1a"

// InitializeServer initializes the server :)
func InitializeServer() {
	LogInfo("Initializing server... " + VERSION)
	http.HandleFunc("/payload", GithubPayload)
	http.HandleFunc("/humor", BotBehaviour)
	http.HandleFunc("/events", SlackEvents)
	http.HandleFunc("/campaign", CampaignHandler)
	http.HandleFunc("/campaign/upload", CampaignUploadHandler)
	http.HandleFunc("/campaign/create", CampaignCreateHandler)
	if err := http.ListenAndServe(":5000", nil); err != nil {
		LogError("Initializing server: " + err.Error())
		os.Exit(1)
	}
}

// AssertFolder asserts we're in the correct folder
func AssertFolder() {
	path, err := os.UserHomeDir()
	if err != nil {
		path = "/home/ec2-user"
	}
	path += "/GOssip-girl"

	if dir, err := os.Getwd(); dir != path || err != nil {
		err := os.Chdir(path)
		handleError(err, "Changing path to the right one")
	}
}

// AssertIsProjectFolder asserts we're in the correct folder
func AssertIsProjectFolder(project string) {
	path, err := os.UserHomeDir()
	if err != nil {
		path = "/home/ec2-user"
	}
	path += "/" + project

	if dir, err := os.Getwd(); dir != path || err != nil {
		err := os.Chdir(path)
		handleError(err, "Changing path to the right one")
	}
}

// SetHeader sets the response's reader
func SetHeader(w http.ResponseWriter) {
	w.Header().Set("Server", "GOssip-girl Server")
	w.Header().Set("Content-Type", "application/json")
}

// HandleError handles the basics errors
func HandleError(w http.ResponseWriter, err error) (bool, bool) {
	if err != nil {
		var syntaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError

		switch {
		case errors.As(err, &syntaxError):
			msg := fmt.Sprintf("Request body contains badly-formed JSON (at position %d)", syntaxError.Offset)
			LogError(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return false, true

		case errors.Is(err, io.ErrUnexpectedEOF):
			msg := fmt.Sprintf("Request body contains badly-formed JSON")
			LogError(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return false, true

		case errors.As(err, &unmarshalTypeError):
			msg := fmt.Sprintf("Request body contains an invalid value for the %q field (at position %d)", unmarshalTypeError.Field, unmarshalTypeError.Offset)
			LogError(msg)
			http.Error(w, msg, http.StatusBadRequest)
			return false, true

		case errors.Is(err, io.EOF):
			LogError(err.Error())
			return true, true

		default:
			LogError(err.Error())
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return false, true
		}
	}
	return true, false
}
