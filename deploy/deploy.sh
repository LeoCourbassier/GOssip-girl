#!/bin/bash
set -e
echo "$PRIVATE_KEY" > permission.pem
chmod 600 permission.pem

./deploy/disableHostCheck.sh

ssh -i permission.pem ec2-user@${DEPLOY_SERVER} 'bash' < ./deploy/updateAndRestart.sh
rm permission.pem
