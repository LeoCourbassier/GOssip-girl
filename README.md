# GOssip-girl

GOssipt-girl é um projeto de um bot feito em GO.

## Features

* Manda mensagem no slack quando houver merge na master
* Cria campanhas para a Hero

## Instalação

### GO
Utilize os docs do [google](https://golang.org/doc/install).

Você precisa ter um compilador para C.

`Clang` no MacOS

`GCC` no Linux

### (Opcional) Instale a extensão pro VSCode
Depois disso, abra o VSCode e instale as ferramentas úteis que ele vai pedir (Vai aparecer um diálogo, é bom ter um linter)

### Instale o Linter

Depois de instalar o GO, coloque no PATH, depois:
```bash
$ go get -u golang.org/x/lint/golint
```

### Criar o arquivo executável

```bash
$ make
```

### Rodar os testes

```bash
$ make test
```

### Rodar o Linter

```bash
$ make lint
```

## Configuração

Baixar o projeto:
```bash
$ git clone git@github.com:quero-edu/GOssip-girl.git
```

Setar as variáveis de ambiente:

```bash
$ export GITHUB_PAYLOAD_SECRET=<secret>
$ export SLACK_API_TOKEN=<secret>
```

Depois disso tudo:

```bash
$ make
$ ./GOssip-girl
```

O bot-servidor será ligado na porta `5000`.

## CI/CD

```bash
$ git remote add gitlab https://gitlab.com/LeoCourbassier/GOssip-girl
```

Aí só dar push que vai rodar o CI/CD, e na master ele já faz deploy.
<br>
<br>
Feito com ❤ pelo squad ~~entusiasta~~ do LLL
