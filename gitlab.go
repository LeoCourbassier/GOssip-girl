package main

import (
	"os"
	"os/exec"

	"github.com/xanzy/go-gitlab"
)

// GetPipelines gets the lastest pipeline url
func GetPipelines(project, branch string) (bool, string) {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if isError := handleError(err, "creating gitlab client"); isError {
		return false, ""
	}

	id := "queroedu/" + project
	info, _, err := git.Pipelines.ListProjectPipelines(id, &gitlab.ListProjectPipelinesOptions{
		Ref:     gitlab.String(branch),
		OrderBy: gitlab.String("updated_at"),
	})
	if isError := handleError(err, "getting pipelines"); isError || len(info) == 0 {
		return false, ""
	}

	shouldShow := info[0].Status == "running" || info[0].Status == "pending"

	return shouldShow, info[0].WebURL
}

// TriggerPipeline triggers a pipeline for a branch already updated
func TriggerPipeline(project, branch string) (bool, string) {
	git, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if isError := handleError(err, "creating gitlab client"); isError {
		return false, ""
	}

	id := "queroedu/" + project
	info, _, err := git.Pipelines.CreatePipeline(id, &gitlab.CreatePipelineOptions{
		Ref: gitlab.String(branch),
	})
	if isError := handleError(err, "creating pipelines"); isError {
		return false, ""
	}

	return true, info.WebURL
}

// FetchGitlab fetches the gitlab updates
func FetchGitlab() {
	out, err := exec.Command("sh", "/home/ec2-user/GOssip-girl/scripts/gitlab_fetch.sh").CombinedOutput()
	LogInfo(string(out))
	if isError := handleError(err, "fetching gitlab"); isError {
		return
	}
}

// PushBranchToGitlab pushes a branch to gitlab
func PushBranchToGitlab(branchName string, project string) (bool, string) {
	AssertIsProjectFolder(project)
	out, err := exec.Command("sh", "/home/ec2-user/GOssip-girl/scripts/gitlab_push.sh", branchName).CombinedOutput()
	LogInfo(string(out))
	if isError := handleError(err, "pushing to gitlab"); isError {
		return false, ""
	}

	return true, string(out)
}
