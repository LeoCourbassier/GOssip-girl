#!/bin/bash

git checkout $1
git pull
git checkout --theirs .
ssh-agent bash -c 'ssh-add ~/.ssh/gitlab; git push gitlab $1'
