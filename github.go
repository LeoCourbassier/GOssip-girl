package main

import (
	"context"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
)

// Committer is the user that made the commit
type Committer struct {
	Name     string
	Email    string
	Username string
}

// Commit is the obj containing information about the commits merged
type Commit struct {
	ID        string
	Message   string
	Timestamp string
	URL       string
	Author    Committer
	Committer Committer
}

// User is the user information from github's payload
type User struct {
	Name  string
	Email string
	ID    int
}

// Repository is the repository information from github's payload
type Repository struct {
	ID       int
	Name     string
	FullName string `json:"full_name"`
	Owner    User
	Size     int
}

// Payload is the obj of github's payload
type Payload struct {
	Ref        string
	Repository Repository
	Pusher     User
	Commits    []Commit
	Compare    string
	HeadCommit Commit `json:"head_commit"`
}

func (payload Payload) toString() string {
	json, err := json.Marshal(payload)
	if err != nil {
		LogError(err.Error())
	}

	return string(json)
}

func (payload Payload) toSlackString() string {
	ref := strings.Split(payload.Ref, "/")
	branch := ref[len(ref)-1]
	linkToProject := "http://www.github.com/" + payload.Repository.FullName
	var commitsStr string

	for _, c := range payload.Commits {
		var cleanMsg string
		if payload.HeadCommit.ID == c.ID {
			cleanMsg = strings.Split(c.Message, "\n\n")[0]
		} else {
			cleanMsg = strings.ReplaceAll(c.Message, "\n", "")
		}
		commitsStr += fmt.Sprintf("\n>`<%s|%s>` - %s", c.URL, c.ID, cleanMsg)
	}

	if payload.HeadCommit.Committer.Username == "web-flow" {
		message := strings.Split(payload.HeadCommit.Message, "\n")[0]

		splitted := strings.Split(message, "from ")

		merge := strings.Split(splitted[0], "#")[1]
		linkToPR := linkToProject + "/pull/" + strings.Trim(merge, " ")

		fromBranchStrings := strings.Split(splitted[1], "/")[1:]
		fromBranch := strings.Join(fromBranchStrings, "/")

		prName := strings.Split(payload.HeadCommit.Message, "\n\n")[1]

		return fmt.Sprintf(">:merged: %s\n> <%s|%d commits> from `%s` to `%s`\n><%s|Pull Request: %s>%s\n>:github: <%s|%s>\n:alan_brisa:", GetUser(payload.Pusher.Name), payload.Compare, len(payload.Commits), fromBranch, branch, linkToPR, prName, commitsStr, linkToProject, payload.Repository.FullName)
	}

	return fmt.Sprintf(">:merged: %s\n> <%s|%d commits> to `%s`%s\n>:github: <%s|%s>\n:alan_brisa:", GetUser(payload.Pusher.Name), payload.Compare, len(payload.Commits), branch, commitsStr, linkToProject, payload.Repository.FullName)
}

func hashPayload(secret string, payload []byte) string {
	hm := hmac.New(sha1.New, []byte(secret))
	hm.Write(payload)
	sum := hm.Sum(nil)
	return fmt.Sprintf("%x", sum)
}

func verifySignature(signatureHeader string, body []byte) bool {
	signatureParts := strings.SplitN(signatureHeader, "=", 2)
	signatureHash := signatureParts[1]

	hash := hashPayload(os.Getenv("GITHUB_PAYLOAD_SECRET"), body)
	LogInfo("Calculated hash: " + hash)

	return hmac.Equal(
		[]byte(hash),
		[]byte(signatureHash),
	)
}

// GithubPayload handles github's calls
func GithubPayload(w http.ResponseWriter, r *http.Request) {
	LogInfo("Receiving a request to /payload")
	SetHeader(w)

	body, _ := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	bodyString := string(body)
	LogInfo("Request body: " + bodyString)

	if r.Method == "POST" {
		githubSignature := r.Header.Get("x-hub-signature")
		if verifySignature(githubSignature, body) {
			LogInfo("Got a request of github payload")
			res := "OK"
			resByte, err := json.Marshal(res)
			HandleError(w, err)

			w.WriteHeader(http.StatusOK)
			w.Write(resByte)

			var payload Payload
			err = json.Unmarshal(body, &payload)

			branch := strings.ReplaceAll(payload.Ref, "refs/heads/", "")

			if IsWhitelistedBranch(branch, payload.Repository.FullName) {
				LogInfo(fmt.Sprintf("Merged in the whitelisted branch %s. Sending message to slack", branch))
				LogInfo(payload.toString())
				LogInfo(payload.toSlackString())

				var message Message
				message.Channel = GetChannel(payload.Repository.FullName)
				message.Text = payload.toSlackString()

				if PostMessage(message) {
					LogInfo("Message delivered to channel " + message.Channel)
				} else {
					LogError("Error delivering the message to channel " + message.Channel)
				}
			}
		} else {
			LogWarning("Wrong password on the /payload: " + githubSignature)
			res := "WTF"
			resByte, err := json.Marshal(res)
			HandleError(w, err)

			w.WriteHeader(http.StatusForbidden)
			w.Write(resByte)
		}

	} else {
		LogWarning("Tried to use GET on the /payload")
		res := "Not the right way to do it"
		resByte, err := json.Marshal(res)
		HandleError(w, err)

		w.WriteHeader(http.StatusForbidden)
		w.Write(resByte)
	}
}

// NewCampaignBranch handle a new branch creation
func NewCampaignBranch(campaignName string, homolog bool) {
	branchName := "campaign-GossipGirlBot-" + campaignName + "-" + GetCurrentTime()

	CreateNewBranch(branchName)
	OptimizeSVGs(campaignName)
	CommitNewBranch(branchName)
	OpenCampaignPullRequest(branchName, campaignName)
	if homolog {
		if err := PushNewBranchToHomolog(branchName); err == nil {
			msg := Message{
				Channel: "CCFUZND62",
				Text:    ":deploy: `homolog`",
			}
			if PostMessage(msg) {
				LogInfo("Mandado mensagem para o canal #deploy-qb-render")
			}
		}
	}
}

// OptimizeSVGs optimizes svgs
func OptimizeSVGs(campaignName string) {
	path := fmt.Sprintf("assets/images/campaigns/%s/hero/all/", campaignName)
	out, err := exec.Command("svgo", "--pretty", path).CombinedOutput()
	LogInfo(string(out))
	handleError(err, "Optimizing svgs")
}

//PushNewBranchToHomolog handle pushing new branch to homolog
func PushNewBranchToHomolog(branchName string) error {
	out, err := exec.Command("git", "checkout", "homolog").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "checkout homolog")

	out, err = exec.Command("git", "pull").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "pull on homolog")

	out, err = exec.Command("git", "merge", branchName).CombinedOutput()
	LogInfo(string(out))
	handleError(err, "merge into homolog")

	out, err = exec.Command("git", "checkout", "--theirs", ".").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "checking out theirs")

	out, err = exec.Command("git", "add", ".").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "add .")

	out, err = exec.Command("git", "commit", "-mfix conflicts").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "committing conflicts")

	out, err = exec.Command("git", "push").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "pushing to origin")

	LogInfo(fmt.Sprintf("Pushed branch %s to homolog \n", branchName))
	return err
}

//OpenCampaignPullRequest handle open PR request
func OpenCampaignPullRequest(branchName string, campaignName string) {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: os.Getenv("BOT_OAUTH")},
	)
	tc := oauth2.NewClient(ctx, ts)
	client := github.NewClient(tc)
	title := "LLL - Campanha automática " + campaignName
	body := `
## Tarefa no Jira
Não tem! @golang_is_superior.

## PR's relacionados

## Mudanças

- Adicionar campanha para ` + campaignName + `
- Deletado (se existir) a ultima campanha com o mesmo slug
- Adicionado as imagens da campanha
- Otimizado SVG
- Alterado campaign-options
- Colocado em homolog

## Checklist
Antes de abrir o PR revise todos estes itens:

- [ ] Adicionei testes para as mudanças realizadas
- [ ] Testes adicionados/modificados passam localmente
- [x] Não adicionei erros novos de validação de html
`

	newPR := &github.NewPullRequest{
		Title:               github.String(title),
		Head:                github.String(branchName),
		Base:                github.String("master"),
		Body:                github.String(body),
		MaintainerCanModify: github.Bool(true),
	}

	pr, _, err := client.PullRequests.Create(ctx, "quero-edu", "qb-render", newPR)
	handleError(err, "creating pull request")

	lllTeamGithubSlug := []string{"lll"}
	reviewers := &github.ReviewersRequest{
		TeamReviewers: lllTeamGithubSlug,
	}

	pr, _, err = client.PullRequests.RequestReviewers(ctx, "quero-edu", "qb-render", *pr.Number, *reviewers)

	handleError(err, "requesting reviewers")

	LogInfo(fmt.Sprintf("PR created: %s\n", pr.GetHTMLURL()))
}

//handleError handle error
func handleError(err error, step string) bool {
	if err != nil {
		LogError("Error in step: " + step)
		LogError(err.Error())
	}

	return err != nil
}

func resetToMaster() {
	out, err := exec.Command("git", "checkout", "master").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "checking out master")

	out, err = exec.Command("git", "pull").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "pull")
}

// CreateNewBranch creates a new branch
func CreateNewBranch(branchName string) {
	out, err := exec.Command("git", "checkout", "-b"+branchName).CombinedOutput()
	LogInfo(string(out))
	handleError(err, "checking out new branch")

	LogInfo(fmt.Sprintf("Created New branch named %s \n", branchName))
}

// CommitNewBranch commits all modified files to the new branch
func CommitNewBranch(branchName string) {
	out, err := exec.Command("git", "add", ".").CombinedOutput()
	LogInfo(string(out))
	handleError(err, "adding stuff to new branch")

	out, err = exec.Command("git", "commit", "-m"+branchName).CombinedOutput()
	LogInfo(string(out))
	handleError(err, "commiting to new branch")

	out, err = exec.Command("git", "push", "--set-upstream", "origin", branchName).CombinedOutput()
	LogInfo(string(out))
	handleError(err, "setting upstream")

	LogInfo(fmt.Sprintf("Commited new changes to %s \n", branchName))
}
