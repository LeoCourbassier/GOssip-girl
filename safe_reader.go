package main

import (
	"bufio"
	"io/ioutil"
	"os"
	"sync"
)

// SafeReader is a safe reader with mutex
type SafeReader struct {
	mutex  *sync.Mutex
	reader *bufio.Reader
	file   *os.File
}

// CreateReader returns a safe reader that has a mutex
func CreateReader(inputFileName string) *SafeReader {
	file, err := os.Open(inputFileName)
	if err != nil {
		LogError(err.Error())
	}

	r := bufio.NewReader(file)

	return &SafeReader{reader: r, mutex: &sync.Mutex{}, file: file}
}

// ReadAll reads everything from a file
func (r *SafeReader) ReadAll() []byte {
	r.mutex.Lock()
	b, err := ioutil.ReadAll(r.reader)
	r.mutex.Unlock()
	if err != nil {
		LogError(err.Error())
		return nil
	}
	return b
}
