package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// Users contains all the translations
var Users UserTranslations

// Channels contains all the channel translations
var Channels ChannelTranslations

// ChannelTranslations is just an array of ChannelTranslation
type ChannelTranslations []ChannelTranslation

// ChannelTranslation maps channels to projects
type ChannelTranslation struct {
	Channel  string   `json:"channel"`
	Project  string   `json:"project"`
	Branches []string `json:"branches"`
}

// UserTranslation is the key for a user
type UserTranslation struct {
	Github string `json:"github"`
	Slack  string `json:"slack"`
}

// UserTranslations is just an array of Translation
type UserTranslations []UserTranslation

// MessageResponse is slack's json response
type MessageResponse struct {
	Ok bool `json:"ok"`
}

// Message is the struct that contains the obj needed to send message
type Message struct {
	Channel      string  `json:"channel"`
	Text         string  `json:"text,omitempty"`
	MsgTimestamp string  `json:"thread_ts"`
	Blocks       []Block `json:"blocks,omitempty"`
}

// Block is a block of the payload
type Block struct {
	Type      string         `json:"type"`
	Text      BlockText      `json:"text"`
	Accessory BlockAccessory `json:"accessory"`
}

// BlockText is the text
type BlockText struct {
	Type string `json:"type"`
	Text string `json:"text"`
}

// BlockAccessory is the accessory of the text
type BlockAccessory struct {
	Type    string `json:"type"`
	Image   string `json:"image_url"`
	AltText string `json:"alt_text"`
}

// LatestPayload is the lastest payload messages
var LatestPayload []SlackPayload

// LatestMessages is the lastest messages
var LatestMessages []Message

// SlackPayload is the obj of the slack's payload
type SlackPayload struct {
	Token     string            `json:"token"`
	Challenge string            `json:"challenge"`
	Type      string            `json:"type"`
	Event     SlackEventPayload `json:"event"`
}

// SlackEventPayload is part of the event api's slack's payload
type SlackEventPayload struct {
	Type           string `json:"type"`
	User           string `json:"user"`
	Text           string `json:"text"`
	MsgTimestamp   string `json:"ts"`
	Channel        string `json:"channel"`
	EventTimestamp string `json:"event_ts"`
}

// ChallengeResponse is the response obj for slack's challenge
type ChallengeResponse struct {
	Challenge string `json:"challenge"`
}

func (t UserTranslations) toString() string {
	json, err := json.Marshal(t)
	if err != nil {
		LogError(err.Error())
	}

	return string(json)
}

func (c ChannelTranslations) toString() string {
	json, err := json.Marshal(c)
	if err != nil {
		LogError(err.Error())
	}

	return string(json)
}

// LoadUsers loads all the translations from the file into array
func LoadUsers() bool {
	body, err := ioutil.ReadFile("users.json")
	if err != nil {
		LogError(err.Error())
		return false
	}

	err = json.Unmarshal(body, &Users)
	if err != nil {
		LogError(err.Error())
		return false
	}

	return true
}

// GetUser returns the slack user given a github user
func GetUser(user string) string {
	for _, u := range Users {
		if u.Github == user {
			return "<@" + u.Slack + ">"
		}
	}

	return user
}

// LoadChannels loads all the channels from the file into array
func LoadChannels() bool {
	body, err := ioutil.ReadFile("channels.json")
	if err != nil {
		LogError(err.Error())
		return false
	}

	err = json.Unmarshal(body, &Channels)
	if err != nil {
		LogError(err.Error())
		return false
	}

	return true
}

// GetChannel returns the chanel given a github project
func GetChannel(project string) string {
	for _, c := range Channels {
		if c.Project == project {
			return c.Channel
		}
	}

	return Channels[0].Channel
}

// IsWhitelistedBranch checks whether a branch is white-listed
func IsWhitelistedBranch(branch string, project string) bool {
	for _, c := range Channels {
		if c.Project == project {
			for _, b := range c.Branches {
				if b == branch || b == "*" {
					return true
				}
			}
			return false
		}
	}

	return false
}

func checkPayload(msg SlackPayload) bool {
	for _, m := range LatestPayload {
		if m == msg {
			return true
		}
	}
	return false
}

func checkSentMessage(msg Message) bool {
	for _, m := range LatestMessages {
		if m.MsgTimestamp != "" && m.MsgTimestamp == msg.MsgTimestamp {
			return true
		}
	}
	return false
}

// PostMessage sends the message to slack
func PostMessage(msg Message) bool {

	if checkSentMessage(msg) {
		LogInfo("Again trying to send an already sent message >(")
		return true
	}

	msgAsJSON, err := json.Marshal(msg)
	if err != nil {
		LogError(err.Error())
		return false
	}

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}

	request, _ := http.NewRequest("POST", "https://slack.com/api/chat.postMessage", bytes.NewBuffer(msgAsJSON))
	request.Header.Set("Content-type", "application/json")
	request.Header.Set("Authorization", "Bearer "+os.Getenv("SLACK_API_TOKEN"))

	resp, err := client.Do(request)
	if err != nil {
		LogError(err.Error())
		return false
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		LogError(err.Error())
		return false
	}
	LogInfo("Payload received: " + string(body))

	var msgResp MessageResponse

	err = json.Unmarshal(body, &msgResp)
	if err != nil {
		LogError(err.Error())
		return false
	}

	LatestMessages = append(LatestMessages, msg)

	return msgResp.Ok
}

// SlackEvents handles the events api from slack
func SlackEvents(w http.ResponseWriter, r *http.Request) {
	AssertFolder()
	LogInfo("Receiving a request to /events")
	SetHeader(w)

	body, err := ioutil.ReadAll(r.Body)
	HandleError(w, err)
	defer r.Body.Close()

	LogInfo("Request body: " + string(body))

	var payload SlackPayload
	err = json.Unmarshal(body, &payload)
	HandleError(w, err)

	if payload.Token != "sCCOAcewz1fHWf4dqB08tcN2" {
		res := "Not yet implemented"
		resByte, err := json.Marshal(res)
		HandleError(w, err)

		w.WriteHeader(http.StatusNoContent)
		w.Write(resByte)
		return
	}

	if payload.Type == "url_verification" {
		var res ChallengeResponse
		res.Challenge = payload.Challenge

		resByte, err := json.Marshal(res)
		HandleError(w, err)

		w.WriteHeader(http.StatusOK)
		w.Write(resByte)
		return
	}

	res := "Ok"
	resByte, err := json.Marshal(res)
	HandleError(w, err)

	w.WriteHeader(http.StatusOK)
	w.Write(resByte)

	if checkPayload(payload) {
		LogInfo("Cancelling processing because of an already received payload")
		return
	}
	LatestPayload = append(LatestPayload, payload)

	if payload.Event.Type == "app_mention" {
		msg := strings.TrimPrefix(payload.Event.Text, "<@U010CHRUW4W>")
		if strings.Contains(msg, "pipeline") {
			msg = strings.ReplaceAll(msg, "\u00a0", " ")
			split := strings.Split(msg, " ")
			index := 1
			if split[0] == "" {
				index++
			}
			repo := split[index]
			branch := split[index+1]

			branch = strings.ReplaceAll(branch, "*", "")
			LogInfo("Trigging pipeline for " + repo + "/" + branch)

			path, err := os.UserHomeDir()
			if _, isError := HandleError(w, err); isError {
				SendErrorMessage(payload.Event)
				return
			}

			path += "/" + repo
			lastPath, err := os.Getwd()
			if err != nil {
				lastPath = "/home/ec2-user/GOssip-girl"
			}
			err = os.Chdir(path)
			if err != nil {
				SendErrorMessage(payload.Event)
				return
			}

			path, err = os.Getwd()
			LogInfo(path)

			FetchGitlab()
			if allCool, _ := PushBranchToGitlab(branch, repo); allCool {
				if succ, pipe := GetPipelines(repo, branch); succ {
					PostMessage(Message{
						Channel:      payload.Event.Channel,
						MsgTimestamp: payload.Event.MsgTimestamp,
						Text:         "<@" + payload.Event.User + "> " + pipe,
					})
				} else {
					if succ, pipe := TriggerPipeline(repo, branch); succ {
						PostMessage(Message{
							Channel:      payload.Event.Channel,
							MsgTimestamp: payload.Event.MsgTimestamp,
							Text:         "<@" + payload.Event.User + "> " + pipe,
						})
					} else {
						SendErrorMessage(payload.Event)
					}
				}
			} else {
				SendErrorMessage(payload.Event)
			}
			os.Chdir(lastPath)
		} else if strings.Contains(msg, "integral") {
			split := strings.Split(msg, " ")
			index := 1
			if split[0] == "" {
				index++
			}
			end, err := strconv.ParseFloat(split[len(split)-1], 64)
			LogInfo(fmt.Sprintf("Ends in %f", end))
			if err != nil {
				LogError("Error to calculate integral of " + err.Error())
				PostMessage(Message{
					Channel:      payload.Event.Channel,
					MsgTimestamp: payload.Event.MsgTimestamp,
					Text:         "O que você tá tentando fazer? :thinkugly:",
				})
				return
			}
			begin, err := strconv.ParseFloat(split[len(split)-2], 64)
			LogInfo(fmt.Sprintf("Begins in %f", begin))
			if err != nil {
				LogError("Error to calculate integral of " + err.Error())
				PostMessage(Message{
					Channel:      payload.Event.Channel,
					MsgTimestamp: payload.Event.MsgTimestamp,
					Text:         "O que você tá tentando fazer? :thinkugly:",
				})
				return
			}
			f := strings.Join(split[index:len(split)-2], "")
			LogInfo(fmt.Sprintf("F is %s", f))

			result, err := Call(1000, f, begin, end)
			if err != nil {
				LogError("Error to calculate integral of " + err.Error())
				PostMessage(Message{
					Channel:      payload.Event.Channel,
					MsgTimestamp: payload.Event.MsgTimestamp,
					Text:         "O que você tá tentando fazer? :thinkugly:",
				})
				return
			}
			LogInfo(fmt.Sprintf("%.4f", result))
			if p, err := UploadFile(payload.Event); err != nil {
				LogError(err.Error())
				SendErrorMessage(payload.Event)
			} else {
				PostMessage(Message{
					Channel:      payload.Event.Channel,
					MsgTimestamp: payload.Event.MsgTimestamp,
					Blocks: []Block{
						{
							Type: "section",
							Accessory: BlockAccessory{
								AltText: "Resultado da integral",
								Image:   p.URL,
								Type:    "image",
							},
							Text: BlockText{
								Text: fmt.Sprintf("Integral é: %.4f", result),
								Type: "mrkdwn",
							},
						},
					},
				})
			}
		}
	}
}

// SendErrorMessage sends an error message
func SendErrorMessage(evt SlackEventPayload) bool {
	errMsg := Message{
		Channel:      evt.Channel,
		MsgTimestamp: evt.MsgTimestamp,
		Text:         "Deu muito ruim. <@UQ9R2426S> veja os meus logs :)",
	}
	return PostMessage(errMsg)
}

// UploadFilePayloadResponse is the response of slack's file upload payload
type UploadFilePayloadResponse struct {
	OK   bool              `json:"ok"`
	File UploadFilePayload `json:"file"`
}

// UploadFilePayload is exactly what it sounds
type UploadFilePayload struct {
	URL string `json:"url_private"`
}

func getFile(f string) (*os.File, error) {
	r, err := os.Open(f)
	if err != nil {
		pwd, _ := os.Getwd()
		fmt.Println("PWD: ", pwd)
		return nil, err
	}
	return r, nil
}

// UploadFile uploads files
func UploadFile(evt SlackEventPayload) (UploadFilePayload, error) {
	var b bytes.Buffer
	var err error
	w := multipart.NewWriter(&b)
	var fw io.Writer
	file, err := getFile("temp/output.png")
	if err != nil {
		handleError(err, "creating os.file")
		return UploadFilePayload{}, err
	}
	if fw, err = w.CreateFormFile("file", file.Name()); err != nil {
		handleError(err, "creating file form")
		return UploadFilePayload{}, err
	}
	if _, err = io.Copy(fw, file); err != nil {
		handleError(err, "copying file")
		return UploadFilePayload{}, err
	}
	if err = w.WriteField("channels", evt.Channel); err != nil {
		handleError(err, "creating channel field form")
		return UploadFilePayload{}, err
	}
	if err = w.WriteField("thread_ts", evt.MsgTimestamp); err != nil {
		handleError(err, "creating thread field form")
		return UploadFilePayload{}, err
	}

	w.Close()

	client := http.Client{
		Timeout: time.Duration(0),
	}

	request, _ := http.NewRequest("POST", "https://slack.com/api/files.upload", &b)
	request.Header.Set("Content-type", w.FormDataContentType())
	request.Header.Set("Authorization", "Bearer "+os.Getenv("SLACK_API_TOKEN"))

	resp, err := client.Do(request)
	if err != nil {
		LogError(err.Error())
		return UploadFilePayload{}, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		LogError(err.Error())
		return UploadFilePayload{}, err
	}
	LogInfo("Payload received: " + string(body))

	var msgResp UploadFilePayloadResponse

	err = json.Unmarshal(body, &msgResp)
	if err != nil {
		LogError(err.Error())
		return UploadFilePayload{}, err
	}

	return msgResp.File, nil
}
