module github.com/leocourbassier/GOssip-girl

go 1.13

require (
	github.com/blend/go-sdk v2.0.0+incompatible // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/google/go-github v17.0.0+incompatible
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	github.com/xanzy/go-gitlab v0.31.0
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8 // indirect
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
)
