package main

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"sync"
	"time"
)

var lastOutputErr error
var outputFile *os.File
var writer *SafeWriter

// SafeWriter is a safe writer with mutex
type SafeWriter struct {
	mutex  *sync.Mutex
	writer *bufio.Writer
	file   *os.File
}

// LogInfo logs info into a file with INFO msg
func LogInfo(msg string) {
	logFile("INFO", msg)
}

// LogError logs error into a file with ERROR msg
func LogError(msg string) {
	logFile("ERR", msg)
}

// LogWarning logs warnings into a file with warning msg
func LogWarning(msg string) {
	logFile("WARN", msg)
}

func logFile(typeLog, msg string) {
	writer.WriteString(fmt.Sprintf("[%s]{%s}: %s\n", GetCurrentTime(), typeLog, msg))
	writer.Flush()
}

// GetCurrentTime returns the current day and time
func GetCurrentTime() string {
	return time.Now().Format("02Jan2006_15h04m05s")
}

func getCurrentDay() string {
	return time.Now().Format("02Jan2006")
}

// CreateWriter returns a new SafeWriter Object
func CreateWriter() {
	var outputFileName string
	_, err := os.Stat("log")
	if os.IsNotExist(err) {
		err = os.Mkdir("log", 0755)
		if err != nil {
			LogError(err.Error())
			outputFileName = fmt.Sprintf("log_%s", getCurrentDay())
		} else {
			outputFileName = filepath.FromSlash(fmt.Sprintf("log/log_%s", getCurrentDay()))
		}
	} else {
		outputFileName = filepath.FromSlash(fmt.Sprintf("log/log_%s", getCurrentDay()))
	}
	outputFile, lastOutputErr = os.OpenFile(outputFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if lastOutputErr != nil {
		fmt.Printf("Error while trying to create output file: %s.\nFile: %s\n", lastOutputErr.Error(), outputFileName)
		os.Exit(1)
	}

	w := bufio.NewWriter(outputFile)

	writer = &SafeWriter{writer: w, mutex: &sync.Mutex{}}
}

// WriteString writes a string to a file
func (w *SafeWriter) WriteString(row string) {
	w.mutex.Lock()
	w.writer.WriteString(row)
	w.mutex.Unlock()
}

// WriteBytes writes a bytes to a file
func (w *SafeWriter) WriteBytes(row []byte) {
	w.mutex.Lock()
	w.writer.Write(row)
	w.mutex.Unlock()
}

// Flush flushes the bytes to a file
func (w *SafeWriter) Flush() {
	w.mutex.Lock()
	w.writer.Flush()
	w.mutex.Unlock()
}

// Close closes the file
func (w *SafeWriter) Close() {
	w.mutex.Lock()
	w.file.Close()
	w.mutex.Unlock()
}
