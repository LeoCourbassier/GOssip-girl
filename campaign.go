package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"sync"
	"text/template"
)

// Status for the current operation
type Status int

// CurrentStatus is the current status
var CurrentStatus Status

const (
	// FoundAll is the status for having find all the files in one search
	FoundAll Status = iota
	// FoundSome is the status for having find some files
	FoundSome Status = iota
	// FoundNone is the status for not having find any file
	FoundNone Status = iota
)

// Campaign is the undergratuation object for campaigns
type Campaign struct {
	Key          string `json:"key"`
	Tag          string `json:"tag"`
	Name         string `json:"name"`
	WeekCampaign bool   `json:"weekCampaign"`
	StartDate    string `json:"startDate"`
	EndDate      string `json:"endDate,omitempty"`
}

// HeroOptions is the options for the campaigns in hero
type HeroOptions struct {
	Options interface{} `json:"options"`
}

// HeroConfiguration holds all the configurations
type HeroConfiguration struct {
	All map[string]*HeroOptions `json:"all"`
}

// SimpleBannerConfigurationOptions is the configuration options for simple banner
type SimpleBannerConfigurationOptions struct {
	Options      interface{} `json:"options"`
	ScrollTarget string      `json:"scrollTarget,omitempty"`
}

// SimpleBannerConfigurationObject holds the configuration options objects
type SimpleBannerConfigurationObject struct {
	Undergraduation SimpleBannerConfigurationOptions `json:"undergraduation"`
	Postgraduation  SimpleBannerConfigurationOptions `json:"postgraduation"`
}

// SimpleBannerConfiguration holds the configuration for the plp and pdp
type SimpleBannerConfiguration struct {
	PDP SimpleBannerConfigurationObject `json:"PDP"`
	PLP SimpleBannerConfigurationObject `json:"PLP"`
}

// TopbarConfigurationOptions is the options configurations for the campaigns in the topbar
type TopbarConfigurationOptions struct {
	Options interface{} `json:"options"`
}

// TopbarConfiguration holds the object with the configurations
type TopbarConfiguration struct {
	Default map[string]*TopbarConfigurationOptions `json:"default"`
}

// TopbarCountdownConfigurationOptions holds the configuration options for the campaigns in the topbarcountdown
type TopbarCountdownConfigurationOptions struct {
	Options []string `json:"options"`
}

// TopbarCountdownConfigurationObject is the object that holds the options
type TopbarCountdownConfigurationObject struct {
	Undergraduation TopbarCountdownConfigurationOptions `json:"undergraduation"`
	Postgraduation  TopbarCountdownConfigurationOptions `json:"postgraduation"`
}

// TopbarCountdownConfiguration holds the configurations
type TopbarCountdownConfiguration struct {
	All TopbarCountdownConfigurationObject `json:"all"`
}

// Badge holds the blacklisted campaigns for the badges
type Badge struct {
	BlackListedCampaigns []string `json:"blacklistedCampaigns"`
}

// Stamp holds the whitelisted campaigns
type Stamp struct {
	AllowedCampaigns []string `json:"allowedCampaigns"`
}

// UrgencyConfigurationOptions is the configurations options for the urgency
type UrgencyConfigurationOptions struct {
	Options string `json:"options"`
}

// UrgencyConfiguration is the holder of the options
type UrgencyConfiguration struct {
	Undergraduation UrgencyConfigurationOptions `json:"undergraduation"`
}

// InlineConfigurationOptions holds the configuration options
type InlineConfigurationOptions struct {
	Options []string `json:"options"`
}

// InlineConfiguration holds the configurations
type InlineConfiguration struct {
	Undergraduation InlineConfigurationOptions `json:"undergraduation"`
	Postgraduation  InlineConfigurationOptions `json:"postgraduation"`
}

// BannerConfiguration holds all the configurations for the campaigns
type BannerConfiguration struct {
	Hero            HeroConfiguration            `json:"hero"`
	SimpleBanner    SimpleBannerConfiguration    `json:"simple-banner"`
	Topbar          TopbarConfiguration          `json:"topbar"`
	TopbarCountdown TopbarCountdownConfiguration `json:"topbar-countdown"`
	Badge           Badge                        `json:"badge"`
	Stamp           Stamp                        `json:"stamp"`
	Urgency         UrgencyConfiguration         `json:"urgency"`
	Inline          InlineConfiguration          `json:"inline"`
}

// SimpleBannerLinkOptionsWithPLP is the simplebanner link options for the plp
type SimpleBannerLinkOptionsWithPLP struct {
	To     string `json:"to,omitempty"`
	Href   string `json:"href,omitempty"`
	Target string `json:"target,omitempty"`
}

// SimpleBannerLinkOptions is the main object with the link options
type SimpleBannerLinkOptions struct {
	To     string                          `json:"to,omitempty"`
	Href   string                          `json:"href,omitempty"`
	Target string                          `json:"target,omitempty"`
	PLP    *SimpleBannerLinkOptionsWithPLP `json:"PLP,omitempty"`
}

// SimpleBanner has all the simple banners
type SimpleBanner struct {
	ImagePathExtension string                  `json:"imagePathExtension"`
	LinkOptions        SimpleBannerLinkOptions `json:"linkOptions"`
	Alt                string                  `json:"alt"`
}

// Topbar has all the topbar campaigns
type Topbar struct {
	BackgroundColor      string `json:"backgroundColor"`
	CampaignName         string `json:"campaignName"`
	Link                 string `json:"link"`
	PrefixText           string `json:"prefixText,omitempty"`
	UnderlinedSuffixText string `json:"underlinedSuffixText,omitempty"`
	TextColor            string `json:"textColor"`
	ShowIcon             *bool  `json:"showIcon,omitempty"`
	SuffixText           string `json:"suffixText,omitempty"`
}

// TopbarCountdown has all the topbarcountdown campaigns
type TopbarCountdown struct {
	Alt                string `json:"alt"`
	BackgroundColor    string `json:"backgroundColor"`
	Color              string `json:"color"`
	ImagePathExtension string `json:"imagePathExtension"`
	Link               string `json:"link"`
}

// InlineLinkOptions is the link option for the inline banner
type InlineLinkOptions struct {
	Href              string `json:"href"`
	To                string `json:"to,omitempty"`
	Target            string `json:"target,omitempty"`
	Rel               string `json:"rel,omitempty"`
	AddSearchLocation *bool  `json:"addSearchLocation,omitempty"`
}

// Inline contains all the inline banners
type Inline struct {
	ImagePathExtension string            `json:"imagePathExtension"`
	ImageName          string            `json:"imageName"`
	LinkOptions        InlineLinkOptions `json:"linkOptions"`
	Click              string            `json:"click,omitempty"`
	Alt                string            `json:"alt"`
}

// Urgency contains all the urgencies campaigns
type Urgency struct {
	BackgroundColor string `json:"backgroundColor"`
	Color           string `json:"color"`
	CampaignTitle   string `json:"campaignTitle"`
	Sign            string `json:"sign"`
}

// Banner contains all the campaigns
type Banner struct {
	Configuration   BannerConfiguration        `json:"configuration"`
	Hero            map[string]Hero            `json:"hero"`
	SimpleBanner    map[string]SimpleBanner    `json:"simple-banner"`
	Topbar          map[string]Topbar          `json:"topbar"`
	Urgency         map[string]Urgency         `json:"urgency"`
	TopbarCountdown map[string]TopbarCountdown `json:"topbar-countdown"`
	Inline          map[string][]Inline        `json:"inline"`
}

// HeroLinkOptionsAll has all the link options for the hero
type HeroLinkOptionsAll struct {
	Href string `json:"href,omitempty"`
	To   string `json:"to,omitempty"`
}

// HeroLinkOptions has the object from all link options
type HeroLinkOptions struct {
	All HeroLinkOptionsAll `json:"all"`
}

// Hero contains all the heroes campaigns
type Hero struct {
	ImagePathExtension   *string          `json:"imagePathExtension"`
	LinkOptions          *HeroLinkOptions `json:"linkOptions"`
	Alt                  *string          `json:"alt"`
	Name                 *string          `json:"name"`
	BackgroundColorLeft  *string          `json:"backgroundColorLeft"`
	BackgroundColorRight *string          `json:"backgroundColorRight"`
}

// CampaignOptions is the campaign options holder
type CampaignOptions struct {
	Campaigns map[string][]Campaign `json:"campaigns"`
	Banners   Banner                `json:"banners"`
}

// CampaignReceivedDetails is the object with the details of a campaign received via get/post from the endpoint
type CampaignReceivedDetails struct {
	ImagePathExtension   string `json:"imagePathExtension"`
	LinkTo               string `json:"to"`
	Alt                  string `json:"alt"`
	Name                 string `json:"name"`
	BackgroundColorLeft  string `json:"backgroundColorLeft"`
	BackgroundColorRight string `json:"backgroundColorRight"`
}

// CampaignReceived contains all the configuration needed for the adding/modifying of a campaign
type CampaignReceived struct {
	Vertical     string                  `json:"vertical"`
	CampaignName string                  `json:"campaignName"`
	Details      CampaignReceivedDetails `json:"details"`
	Append       bool                    `json:"appendCampaign"`
	FileName     string                  `json:"fileName"`
	SvgFile      string                  `json:"svgFile"`
	Homolog      bool                    `json:"homolog"`
}

// CampaignHandler does the handling of adding/modifying a campaign
func CampaignHandler(w http.ResponseWriter, r *http.Request) {
	SetHeader(w)

	user := getUser()
	dir, err := os.Getwd()
	HandleError(w, err)
	os.Chdir(filepath.Dir(user+"/../") + "/qb-render/")
	resetToMaster()
	os.Chdir(dir)

	path := filepath.Dir(user+"/../") + "/qb-render/assets/data/campaign-options.json"
	reader := CreateReader(path)
	defer reader.file.Close()

	body, err := ioutil.ReadAll(r.Body)
	HandleError(w, err)
	defer r.Body.Close()

	var campaignOptionsFile []byte
	campaignOptionsFile = reader.ReadAll()

	campaignOptions, homolog, err := addOrModifyCampaign(body, campaignOptionsFile)
	HandleError(w, err)

	res, err := json.Marshal(campaignOptions)
	HandleError(w, err)

	if test(campaignOptionsFile, res) {
		fmt.Println("Nada feito! Abortando")
	} else {
		jsonOut := filepath.FromSlash(fmt.Sprintf("campaign-options_%s.json", GetCurrentTime()))
		out := createFile(jsonOut)
		buffer := new(bytes.Buffer)
		enc := json.NewEncoder(buffer)

		enc.SetEscapeHTML(false)
		enc.SetIndent("", "  ")
		err = enc.Encode(campaignOptions)
		HandleError(w, err)

		out.WriteBytes(buffer.Bytes())
		out.Close()

		campaignName := handleZip(body, w)
		if campaignName == "" {
			LogError("Error trying to create new campaign")
			return
		}

		backupJSON(jsonOut + ".backup")
		if err := moveJSON(jsonOut); err == nil {
			prepareToOpenPR(campaignName, homolog)
		} else {
			HandleError(w, err)
		}
	}

	os.Chdir(dir)
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func prepareToOpenPR(campaignName string, homolog bool) {
	user := getUser()
	path := filepath.Dir(user+"/../") + "/qb-render/"
	os.Chdir(path)

	NewCampaignBranch(campaignName, homolog)
}

func addOrModifyCampaign(received, campaignOptionsFile []byte) (CampaignOptions, bool, error) {
	var campaignOptions CampaignOptions
	var campaignToModify CampaignReceived

	err := json.Unmarshal(campaignOptionsFile, &campaignOptions)
	if err != nil {
		return CampaignOptions{}, false, err
	}

	err = json.Unmarshal(received, &campaignToModify)
	if err != nil {
		return CampaignOptions{}, false, err
	}

	if campaignOptions.Banners.Configuration.Hero.All[campaignToModify.Vertical] == nil {
		campaignOptions.Banners.Configuration.Hero.All[campaignToModify.Vertical] = &HeroOptions{
			Options: []string{campaignToModify.CampaignName},
		}
	} else {
		options := &campaignOptions.Banners.Configuration.Hero.All[campaignToModify.Vertical].Options

		switch reflect.TypeOf(*options).Kind() {
		case reflect.String:
			*options = campaignToModify.CampaignName
		case reflect.Slice:
			slice := reflect.ValueOf(*options)

			for i := 0; i < slice.Len(); i++ {
				if slice.Index(i).Interface() == campaignToModify.CampaignName {
					break
				}
			}

			if campaignToModify.Append {
				temp := []string{campaignToModify.CampaignName}
				for i := 0; i < slice.Len(); i++ {
					temp = append(temp, slice.Index(i).Elem().String())
				}
				*options = temp
			} else {
				*options = []string{campaignToModify.CampaignName}
			}
		default:
			*options = []string{campaignToModify.CampaignName}
		}
	}

	if campaignOptions.Banners.Hero[campaignToModify.CampaignName] == (Hero{}) {
		campaignOptions.Banners.Hero[campaignToModify.CampaignName] = Hero{
			Alt:                  &campaignToModify.Details.Alt,
			BackgroundColorLeft:  &campaignToModify.Details.BackgroundColorLeft,
			BackgroundColorRight: &campaignToModify.Details.BackgroundColorRight,
			ImagePathExtension:   &campaignToModify.Details.ImagePathExtension,
			Name:                 &campaignToModify.Details.Name,
			LinkOptions: &HeroLinkOptions{
				All: HeroLinkOptionsAll{
					To: campaignToModify.Details.LinkTo,
				},
			},
		}
	} else {
		campaign := campaignOptions.Banners.Hero[campaignToModify.CampaignName]

		*campaign.Alt = campaignToModify.Details.Alt
		*campaign.BackgroundColorLeft = campaignToModify.Details.BackgroundColorLeft
		*campaign.BackgroundColorRight = campaignToModify.Details.BackgroundColorRight
		*campaign.ImagePathExtension = campaignToModify.Details.ImagePathExtension
		*campaign.Name = campaignToModify.Details.Name

		if campaign.LinkOptions.All.Href == "" {
			campaign.LinkOptions.All.To = campaignToModify.Details.LinkTo
		} else {
			campaign.LinkOptions.All.Href = campaignToModify.Details.LinkTo
		}
	}

	return campaignOptions, campaignToModify.Homolog, nil
}

func createFile(outputFileName string) *SafeWriter {
	outputFile, lastOutputErr = os.Create(outputFileName)
	if lastOutputErr != nil {
		LogError(fmt.Sprintf("Error while trying to create output file: %s.\nFile: %s\n", lastOutputErr.Error(), outputFileName))
	}

	w := bufio.NewWriter(outputFile)

	return &SafeWriter{writer: w, mutex: &sync.Mutex{}}
}

func test(a, b []byte) bool {
	var o1 interface{}
	var o2 interface{}

	err := json.Unmarshal(a, &o1)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	err = json.Unmarshal(b, &o2)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	return reflect.DeepEqual(o1, o2)
}

// CampaignUploadHandler handles a file that will be later used as the campaign images
func CampaignUploadHandler(w http.ResponseWriter, r *http.Request) {
	AssertFolder()
	switch r.Method {
	case "GET":
		file := path.Join("templates", "upload_file.html")
		template, err := template.ParseFiles(file)
		HandleError(w, err)

		err = template.Execute(w, nil)
		HandleError(w, err)
		break
	case "POST":
		handleUpload(w, r)
		break
	default:
		w.Write([]byte("?"))
	}
}

// CampaignCreateHandler creates the campaign and calls the handler
func CampaignCreateHandler(w http.ResponseWriter, r *http.Request) {
	AssertFolder()

	switch r.Method {
	case "GET":
		file := path.Join("templates", "create_campaign.html")
		template, err := template.ParseFiles(file)
		HandleError(w, err)

		err = template.Execute(w, nil)
		HandleError(w, err)
		break
	case "POST":
		var campaign CampaignReceived
		campaign.Vertical = r.FormValue("vertical")
		campaign.CampaignName = r.FormValue("campaignName")
		campaign.Append = r.FormValue("appendCampaign") == "on"
		campaign.FileName = r.FormValue("fileName")
		campaign.SvgFile = r.FormValue("svgFile")
		campaign.Details.ImagePathExtension = r.FormValue("imagePathExtension")
		campaign.Details.LinkTo = r.FormValue("to")
		campaign.Details.Alt = r.FormValue("alt")
		campaign.Details.Name = r.FormValue("name")
		campaign.Details.BackgroundColorLeft = r.FormValue("backgroundColorLeft")
		campaign.Details.BackgroundColorRight = r.FormValue("backgroundColorRight")
		campaign.Homolog = r.FormValue("homolog") == "on"

		buffer := new(bytes.Buffer)
		enc := json.NewEncoder(buffer)

		enc.SetEscapeHTML(false)
		enc.SetIndent("", "  ")
		err := enc.Encode(campaign)
		HandleError(w, err)

		file := path.Join("templates", "make_magic.html")
		template, err := template.ParseFiles(file)
		HandleError(w, err)

		var t Test
		t.Response = string(buffer.Bytes())
		err = template.Execute(w, t)
		HandleError(w, err)
		break
	default:
		w.Write([]byte("?"))
	}
}

// Test just helps to marshall to make the request
type Test struct {
	Response string
}

func handleUpload(w http.ResponseWriter, r *http.Request) {
	SetHeader(w)
	r.ParseMultipartForm(10 << 20)
	file, handler, err := r.FormFile("campaign")
	HandleError(w, err)
	defer file.Close()

	ext := strings.Split(handler.Filename, ".")[1]
	tempFile, err := ioutil.TempFile("campaigns", "campaign-*."+ext)
	HandleError(w, err)
	defer tempFile.Close()

	fileBytes, err := ioutil.ReadAll(file)
	HandleError(w, err)

	tempFile.Write(fileBytes)
	w.Write([]byte("Upload feito com sucesso!\n"))
	filter := strings.Split(tempFile.Name(), "campaigns/")[1]
	w.Write([]byte("O nome do arquivo é:\n" + filter + "\nAnote!"))
}

func searchForExactFiles(path string, files []string, needMatchAll bool) ([]string, error) {
	info, err := ioutil.ReadDir(path)
	if err == nil {
		names := []string{}
		numFolders := 0
		folderIndex := 0
		for i, file := range info {
			names = append(names, file.Name())
			if file.IsDir() {
				numFolders++
				folderIndex = i
			}
		}

		foundFiles := hasAllFiles(names, files, path)
		margin := 0
		if !needMatchAll {
			margin = 2
		}
		if len(foundFiles) < len(files)-margin {
			if numFolders == 0 || numFolders > 1 {
				return nil, errors.New("Too much folders or none")
			}

			path += "/" + info[folderIndex].Name()
			return searchForExactFiles(path, files, needMatchAll)
		}

		if len(foundFiles) > len(files)-margin {
			CurrentStatus = FoundAll
		} else {
			CurrentStatus = FoundSome
		}
		return foundFiles, nil
	}

	CurrentStatus = FoundNone
	return nil, err
}

// FileRename is a struct that will help with the renaming
type FileRename struct {
	Name string
	File string
}

func searchForWordFiles(path string, ext string) ([]FileRename, error) {
	info, err := ioutil.ReadDir(path)
	mapFile := []FileRename{}
	if err == nil {
		numFolders := 0
		folderIndex := 0
		for i, file := range info {
			if file.IsDir() {
				numFolders++
				folderIndex = i
			}
		}

		hasCompactedFolder := false
		for _, file := range info {
			fileName := strings.ToLower(file.Name())
			if strings.Contains(fileName, "compactado") {
				hasCompactedFolder = true
				path += "/" + file.Name()
			}
		}
		if hasCompactedFolder {
			info, err = ioutil.ReadDir(path)
			if err != nil {
				return nil, err
			}

			foundFiles := 0
			for _, file := range info {
				// desktop-banner
				test := []string{
					"home", "desktop", "hero", "novo", "1x",
				}
				proposal := strings.Count(strings.ToLower(file.Name()), "novo")
				if proposal == 1 {
					if hasAllWords(file.Name(), test) {
						LogInfo("Found desktop-banner.png")
						foundFiles++
						mapFile = append(mapFile, FileRename{
							File: path + "/" + file.Name(),
							Name: "desktop-banner." + ext,
						})
					}
				} else if proposal == 2 {
					if hasAllWords(file.Name(), test) {
						LogInfo("Found desktop_570px-banner.png")
						foundFiles++
						mapFile = append(mapFile, FileRename{
							File: path + "/" + file.Name(),
							Name: "desktop_570px-banner." + ext,
						})
					}
				}

				// desktop@2x-banner
				test = []string{
					"home", "desktop", "hero", "novo", "2x",
				}
				if proposal == 1 {
					if hasAllWords(file.Name(), test) {
						LogInfo("Found desktop@2x-banner.png")
						foundFiles++
						mapFile = append(mapFile, FileRename{
							File: path + "/" + file.Name(),
							Name: "desktop@2x-banner." + ext,
						})
					}
				} else if proposal == 2 {
					if hasAllWords(file.Name(), test) {
						LogInfo("Found desktop@2x_570px-banner.png")
						foundFiles++
						mapFile = append(mapFile, FileRename{
							File: path + "/" + file.Name(),
							Name: "desktop@2x_570px-banner." + ext,
						})
					}
				}

				// mobile-banner
				test = []string{
					"home", "mobile", "hero", "1x",
				}
				if hasAllWords(file.Name(), test) {
					LogInfo("Found mobile-banner.png")
					foundFiles++
					mapFile = append(mapFile, FileRename{
						File: path + "/" + file.Name(),
						Name: "mobile-banner." + ext,
					})
				}

				// mobile@2x-banner
				test = []string{
					"home", "mobile", "hero", "2x",
				}
				if hasAllWords(file.Name(), test) {
					LogInfo("Found mobile@2x-banner.png")
					foundFiles++
					mapFile = append(mapFile, FileRename{
						File: path + "/" + file.Name(),
						Name: "mobile@2x-banner." + ext,
					})
				}

				// mobile-content
				test = []string{
					"home", "svg", "mobile",
				}
				if hasAllWords(file.Name(), test) {
					LogInfo("Found mobile-content.svg")
					foundFiles++
					mapFile = append(mapFile, FileRename{
						File: path + "/" + file.Name(),
						Name: "mobile-content.svg",
					})
				}

				// desktop-content.svg
				test = []string{
					"home", "svg", "desktop",
				}
				if hasAllWords(file.Name(), test) {
					LogInfo("Found desktop-content.svg")
					foundFiles++
					mapFile = append(mapFile, FileRename{
						File: path + "/" + file.Name(),
						Name: "desktop-content.svg",
					})
				}
			}

			if foundFiles >= 6 {
				CurrentStatus = FoundAll
				if foundFiles == 6 {
					CurrentStatus++
				}
			} else {
				CurrentStatus = FoundNone
			}

			return mapFile, nil
		}

		if numFolders == 0 || numFolders > 1 {
			return nil, errors.New("Too much folders or none")
		}

		path += "/" + info[folderIndex].Name()
		return searchForWordFiles(path, ext)
	}

	return nil, err
}

func searchForWordFiles2(path string) ([]FileRename, error) {
	info, err := ioutil.ReadDir(path)
	mapFile := []FileRename{}
	if err == nil {
		numFolders := 0
		folderIndex := 0
		for i, file := range info {
			if file.IsDir() {
				numFolders++
				folderIndex = i
			}
		}

		foundFiles := 0
		for _, file := range info {
			// mobile-content
			test := []string{
				"home", "svg", "mobile",
			}
			if hasAllWords(file.Name(), test) {
				LogInfo("Found mobile-content.svg")
				foundFiles++
				mapFile = append(mapFile, FileRename{
					File: path + "/" + file.Name(),
					Name: "mobile-content.svg",
				})
			}

			// desktop-content.svg
			test = []string{
				"home", "svg", "desktop",
			}
			if hasAllWords(file.Name(), test) {
				LogInfo("Found desktop-content.svg")
				foundFiles++
				mapFile = append(mapFile, FileRename{
					File: path + "/" + file.Name(),
					Name: "desktop-content.svg",
				})
			}
		}

		if foundFiles >= 2 {
			return mapFile, nil
		}

		if numFolders == 0 || numFolders > 1 {
			return nil, errors.New("Too much folders or none")
		}

		path += "/" + info[folderIndex].Name()
		return searchForWordFiles2(path)
	}

	return nil, err
}

func handleZip(r []byte, w http.ResponseWriter) string {
	AssertFolder()

	CurrentStatus = FoundNone
	var campaign CampaignReceived
	err := json.Unmarshal(r, &campaign)
	HandleError(w, err)
	path := filepath.FromSlash(fmt.Sprintf("campaigns/%s", campaign.FileName))
	output := filepath.FromSlash(fmt.Sprintf("temp/%s", strings.Split(campaign.FileName, ".")[0]))

	_, err = os.Stat(path)
	if os.IsExist(err) || err == nil {
		_, err := Unzip(path, output)
		if _, isError := HandleError(w, err); isError {
			return ""
		}

		correctNames := []string{
			"desktop-banner." + campaign.Details.ImagePathExtension,
			"desktop@2x-banner." + campaign.Details.ImagePathExtension,
			"mobile-banner." + campaign.Details.ImagePathExtension,
			"mobile@2x-banner." + campaign.Details.ImagePathExtension,
			"desktop_570px-banner." + campaign.Details.ImagePathExtension,
			"desktop@2x_570px-banner." + campaign.Details.ImagePathExtension,
			"desktop-content.svg",
			"mobile-content.svg",
		}

		user := getUser()
		deletePath := filepath.Dir(user+"/../") + "/qb-render/assets/images/campaigns/" + campaign.CampaignName + "/hero/all/"
		err = os.RemoveAll(deletePath)
		handleError(err, "Remove last folders")

		if foundFiles, err := searchForExactFiles(output, correctNames, false); err == nil {
			err := moveAllFiles(foundFiles, campaign.CampaignName)
			if _, isError := HandleError(w, err); isError {
				return ""
			}
		} else {
			if renames, err := searchForWordFiles(output, campaign.Details.ImagePathExtension); err == nil {
				err := moveAndRenameFiles(renames, campaign.CampaignName)
				if _, isError := HandleError(w, err); isError {
					return ""
				}
			} else {
				if _, isError := HandleError(w, err); isError {
					return ""
				}
			}
		}

		if CurrentStatus == FoundSome {
			path := filepath.FromSlash(fmt.Sprintf("campaigns/%s", campaign.SvgFile))
			output := filepath.FromSlash(fmt.Sprintf("temp/%s", strings.Split(campaign.SvgFile, ".")[0]))

			_, err = os.Stat(path)
			if os.IsExist(err) || err == nil {
				_, err := Unzip(path, output)
				if _, isError := HandleError(w, err); isError {
					return ""
				}
			}

			correctNames := []string{
				"desktop-content.svg",
				"mobile-content.svg",
			}

			if foundFiles, err := searchForExactFiles(output, correctNames, true); err == nil {
				err := moveAllFiles(foundFiles, campaign.CampaignName)
				if _, isError := HandleError(w, err); isError {
					return ""
				}
			} else {
				if renames, err := searchForWordFiles2(output); err == nil {
					err := moveAndRenameFiles(renames, campaign.CampaignName)
					if _, isError := HandleError(w, err); isError {
						return ""
					}
				} else {
					if _, isError := HandleError(w, err); isError {
						return ""
					}
				}
			}
		}

	} else if os.IsNotExist(err) {
		LogError(fmt.Sprintf("%s não existe!", path))
		LogError(err.Error())
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Arquivo não encontrado!"))
		return ""
	} else {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Arquivo não encontrado!"))
		return ""
	}
	return campaign.CampaignName
}

func hasAllWords(toSearch string, words []string) bool {
	for _, w := range words {
		if !strings.Contains(strings.ToLower(toSearch), w) {
			return false
		}
	}

	return true
}

func hasAllFiles(toSearch, toFind []string, path string) []string {
	foundFiles := []string{}
	for _, f := range toFind {
		for _, w := range toSearch {
			if f == w {
				foundFiles = append(foundFiles, path+"/"+f)
				break
			}
		}
	}
	return foundFiles
}

func getUser() string {
	_, filename, _, _ := runtime.Caller(0)
	return filepath.Dir(filename)
}

func moveAllFiles(files []string, campaignName string) error {
	user := getUser()
	path := filepath.Dir(user+"/../") + "/qb-render/assets/images/campaigns/" + campaignName + "/hero/all/"
	os.MkdirAll(path, os.ModePerm)
	for i := range files {
		split := strings.Split(files[i], "/")
		name := split[len(split)-1]

		out := path + name
		LogInfo("Moving " + files[i] + " to " + out)
		err := os.Rename(files[i], out)
		if err != nil {
			LogError(err.Error())
			return err
		}
	}

	return nil
}

func moveJSON(file string) error {
	user := getUser()
	path := filepath.Dir(user+"/../") + "/qb-render/assets/data/campaign-options.json"
	LogInfo("Moving " + file + " to " + path)
	err := os.Rename(file, path)
	if err != nil {
		LogError(err.Error())
		return err
	}

	return nil
}

func backupJSON(file string) error {
	user := getUser()
	path := filepath.Dir(user+"/../") + "/qb-render/assets/data/campaign-options.json"
	LogInfo("Moving " + path + " to " + file)
	err := os.Rename(path, file)
	if err != nil {
		LogError(err.Error())
		return err
	}

	return nil
}

func moveAndRenameFiles(rename []FileRename, campaignName string) error {
	user := getUser()
	path := filepath.Dir(user+"/../") + "/qb-render/assets/images/campaigns/" + campaignName + "/hero/all/"
	os.MkdirAll(path, os.ModePerm)
	for _, r := range rename {
		out := path + r.Name
		LogInfo("Moving " + r.File + " to " + out)
		err := os.Rename(r.File, out)
		if err != nil {
			LogError(err.Error())
			return err
		}
	}

	return nil
}
