## Tarefa no Jira
Link da tarefa aqui (com /browse).

## PRs relacionados
Links para PRs criados relacionados com a feature nesse ou em outro(s) projeto(s).

## Mudanças
Lista com as mudanças importantes realizadas.
- Mudança 1

## Checklist
Antes de abrir o PR revise todos estes itens:

- [ ] Adicionei testes para as mudanças realizadas
- [ ] Testes adicionados/modificados passam localmente

## Pipeline Gitlab

### `make test`

### `make lint`
