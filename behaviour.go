package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

// Humor is the object that will hold the possible humors
type Humor struct {
	Description string   `json:"description"`
	Lines       []string `json:"lines"`
	Probability float64  `json:"probability"`
}

// Humors is an array of humor
type Humors []Humor

// Behaviours is an instantiation of the Humors type
var Behaviours Humors

// CurrentHumor holds the current mood for the bot
var CurrentHumor Humor

// IntArray is just an array of ints :)
type IntArray []int

func convertToBinary(str string) string {
	res := ""
	for _, c := range str {
		res = fmt.Sprintf("%s%.8b", res, c)
	}
	return res
}

func (i IntArray) find(index int) int {
	for j := 0; j < len(i); j++ {
		if i[j] == index {
			return j
		}
	}
	return -1
}

func stringToBin(s string) (binString string) {
	for _, c := range s {
		binString = fmt.Sprintf("%s%b", binString, c)
	}
	return
}

func (h Humors) sum() int {
	sum := 0
	for _, hm := range h {
		sum += int(hm.Probability * 100)
	}
	return sum
}

func (h Humors) partialSum(index int) int {
	sum := 0
	for i := 0; i <= index; i++ {
		sum += int(h[i].Probability * 100)
	}
	return sum
}

func (h Humor) parseString(str string) string {
	switch h.Description {
	case "malfunction":
		words := strings.Split(str, " ")
		corrupted := rand.Intn(len(words)) + 1
		var sortedIndexes IntArray

		for i := 0; i < corrupted; i++ {
			sorted := rand.Intn(len(words))
			if sortedIndexes.find(sorted) > -1 {
				i--
				continue
			}

			sortedIndexes = append(sortedIndexes, sorted)
			words[sorted] = convertToBinary(words[sorted])
		}

		str = strings.Join(words, " ")
	case "happy":
		str += "\n /\\_/\\\n( o.o )\n > ^ <\n"
	}

	return str
}

// LoadBehaviours loads all the behaviours from the json
func LoadBehaviours() bool {
	body, err := ioutil.ReadFile("humors.json")
	if err != nil {
		LogError(err.Error())
		return false
	}

	err = json.Unmarshal(body, &Behaviours)
	if err != nil {
		LogError(err.Error())
		return false
	}

	InitializeBehaviour()
	return true
}

// BotBehaviour is the endpoint responsable for the bot's humor
func BotBehaviour(w http.ResponseWriter, r *http.Request) {
	SetHeader(w)
	if r.Method == "POST" {
		InitializeBehaviour()
		random := rand.Intn(len(CurrentHumor.Lines))
		res := "<!here> " + CurrentHumor.parseString(CurrentHumor.Lines[random])
		resByte := []byte(res)

		w.WriteHeader(http.StatusOK)
		w.Write(resByte)

		var message Message
		message.Channel = Channels[0].Channel
		message.Text = res
		PostMessage(message)
	} else {
		LogWarning("Tried to use GET on the /humor")
		res := "Not the right way to do it"
		resByte, err := json.Marshal(res)
		HandleError(w, err)

		w.WriteHeader(http.StatusForbidden)
		w.Write(resByte)
	}
}

// InitializeBehaviour sets a behaviour for the bot
func InitializeBehaviour() {
	rand.Seed(time.Now().UnixNano())
	sortedBehaviour := rand.Intn(100) + 1

	for i := range Behaviours {
		if Behaviours.partialSum(i) > sortedBehaviour {
			CurrentHumor = Behaviours[i]
			break
		}
	}

	LogInfo("Current Humor: " + CurrentHumor.Description)
}
